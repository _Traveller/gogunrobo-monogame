﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GoGunRobo_Mono
{
    class Entity
    {
        // Reference to the game's SpriteBatch
        protected static SpriteBatch batchRef;

        protected Vector2 position;
        protected Texture2D texture;
        protected Color color;

        public Entity()
        {
            position = Vector2.Zero;
            color = Color.White;
        }

        public Entity(Vector2 pos)
        {
            position = pos;
            color = Color.White;
        }

        public void Update()
        {
            // Overwrite
        }

        public void Draw()
        {
            batchRef.Draw(texture, position, color);
        }

        public void Draw(Color newColor)
        {
            batchRef.Draw(texture, position, newColor);
        }

        // Checks if any collision is made with the Other given entity
        public bool DidCollide(Entity other)
        {
            // If they overlap at all, return true for collision
            if (other.Position.X < position.X + texture.Width &&
                other.Position.Y + other.Texture.Height > position.Y &&
                other.Position.X + other.Texture.Width > position.X &&
                position.Y + texture.Width > other.Position.Y)
                    return true;

            // Otherwise, just return false
            return false;
        }

        // Only ever need to grab the SpriteBatch Reference
        // static so it can easily be shared to all offspring
        public static SpriteBatch BatchReference
        {
            set { batchRef = value; }
        }

        // Only need to grab it's texture
        public Texture2D Texture
        {
            get { return texture; }
            set { texture = value; }
        }

        // Update Entity's position
        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }
    }
}
