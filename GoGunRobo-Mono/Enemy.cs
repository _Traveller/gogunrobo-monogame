﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GoGunRobo_Mono
{
    class Enemy : Entity
    {
        private BulletManager bulletManagerRef;

        private bool flyUp;

        public Enemy() {
            flyUp = false;
        }

        public Enemy(Vector2 pos) : base(pos) {
            flyUp = false;
        }

        public void Update(GameTime gameTime)
        {
            AIKite(gameTime);
        }

        // AI for kiting the player: Drop in, shoot, and pop out
        private void AIKite(GameTime gameTime)
        {
            //Bob onto the screen
            if (!flyUp) position.Y += 2f;
            else position.Y -= 2f;

            if (position.Y == 80f)
            {
                bulletManagerRef.AddNewBullet(position);
                flyUp = true;
            }

        }

        public BulletManager BulletManagerRef
        {
            set { bulletManagerRef = value; }
        }
    }
}
