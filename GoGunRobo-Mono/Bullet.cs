﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GoGunRobo_Mono
{
    class Bullet : Entity
    {
        // The direction the bullet is supposed to head towards
        private Vector2 direction;

        // Easy "Just Head Downwards" shot
        public Bullet(Vector2 initialPos)
        {
            position = initialPos;
            direction = new Vector2(0, 1);
        }

        public Bullet(Vector2 initialPos, Vector2 shotDirection)
        {
            position = initialPos;
            direction = shotDirection;
        }

        // Keep moving in the direction it was shot at
        public new void Update()
        {
            position += direction;
        }
    }
}
