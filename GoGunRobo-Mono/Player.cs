﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GoGunRobo_Mono
{
    class Player : Entity
    {
        // Reference to our bullet manager
        private BulletManager bulletManagerRef;
        // References to the game's keyboard and gamepad state
        private KeyboardState keyStateRef;
        private GamePadState padStateRef;
        // Pixel-Perfect Movement constant
        private const float SPEED = 1f;
        // For keeping the player in bounds of the screen
        private float screenWidth, screenHeight;
        // So the player can't continuosly shoot if they hold the shoot button
        private bool heldDown;

        public Player()
        {
            heldDown = false;
        }

        public Player(Vector2 pos) : base(pos)
        {
            heldDown = false;
        }

        // Overriding Entity's
        public new void Update()
        {
            HandleControls();
            BoundaryCheck();
        }

        private void HandleControls()
        {
            // Movement
            if (padStateRef.ThumbSticks.Left.X > 0 ||
                keyStateRef.IsKeyDown(Keys.Right))
                position.X += SPEED;
            if (padStateRef.ThumbSticks.Left.X < 0 ||
                keyStateRef.IsKeyDown(Keys.Left))
                position.X -= SPEED;
            if (padStateRef.ThumbSticks.Left.Y > 0 ||
                keyStateRef.IsKeyDown(Keys.Up))
                position.Y -= SPEED;
            if (padStateRef.ThumbSticks.Left.Y < 0 ||
                keyStateRef.IsKeyDown(Keys.Down))
                position.Y += SPEED;

            // Shooting
            if (padStateRef.Buttons.A == ButtonState.Pressed ||
                keyStateRef.IsKeyDown(Keys.Z))
            {
                if (!heldDown)
                {
                    bulletManagerRef.AddNewBullet(this.position, new Vector2(0, -1));
                    heldDown = true;
                }
            }
            else if (padStateRef.Buttons.A == ButtonState.Released ||
                keyStateRef.IsKeyUp(Keys.Z))
                heldDown = false;
        }

        public void AssignBoundary(float windowWidth, float windowHeight)
        {
            screenWidth = windowWidth;
            screenHeight = windowHeight;
        }

        // Checks to keep the player within the screen
        private void BoundaryCheck()
        {
            if (position.X < 0) position.X = 0;
            if (position.X > screenWidth - this.texture.Width) position.X = screenWidth - this.texture.Width;
            if (position.Y < 0) position.Y = 0;
            if (position.Y > screenHeight - this.texture.Height) position.Y = screenHeight - this.texture.Height;
        }

        public KeyboardState KeyState
        {
            set { keyStateRef = value; }
        }

        public GamePadState PadState
        {
            set { padStateRef = value; }
        }

        public BulletManager BulletManagerReference
        {
            set { bulletManagerRef = value; }
        }
    }
}
