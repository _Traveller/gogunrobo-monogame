﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace GoGunRobo_Mono
{
    class BulletManager
    {
        // The actual width and height of our game window
        int viewportWidth, viewportHeight;

        //====Bullet Pool====
        const int BULLET_LIMIT = 1000;
        int bulletAmount;
        Bullet[] bulletCache;

        //====Bullet Textures====
        const int TEXTURES_AMOUNT = 1; // Currently only one prototype texture
        Texture2D[] bulletTextures;
        enum BulletSkins { PROTOTYPE }
        int textureCount;

        public BulletManager()
        {
            bulletAmount = textureCount = -1;
            bulletCache = new Bullet[BULLET_LIMIT];
            bulletTextures = new Texture2D[TEXTURES_AMOUNT];
        }

        public void SetViewportBounds(int width, int height)
        {
            viewportWidth = width;
            viewportHeight = height;
        }

        public void LoadTextures(Texture2D tex)
        {
            textureCount++;
            if (textureCount != TEXTURES_AMOUNT) bulletTextures[textureCount] = tex;
        }

        public void AddNewBullet(Vector2 shooterPos)
        {
            bulletAmount++;
            // If we hit our cache limit, overwrite the bullets, starting from the first
            if (bulletAmount == BULLET_LIMIT - 1) bulletAmount = 0;
            bulletCache[bulletAmount] = new Bullet(shooterPos);
            bulletCache[bulletAmount].Texture = bulletTextures[(int)BulletSkins.PROTOTYPE];
            // Console.WriteLine(bulletAmount);
        }

        public void AddNewBullet(Vector2 shooterPos, Vector2 shotDirection)
        {
            bulletAmount++;
            if (bulletAmount == BULLET_LIMIT - 1) bulletAmount = 0;
            bulletCache[bulletAmount] = new Bullet(shooterPos, shotDirection);
            bulletCache[bulletAmount].Texture = bulletTextures[(int)BulletSkins.PROTOTYPE];
        }

        public void UpdateAllBullets()
        {
            for (int i = 0; i <= bulletAmount; i++)
            {
                if(!OutOfBounds(i))
                    bulletCache[i].Update();
            }
        }

        public void DrawAllBullets()
        {
            for (int i = 0; i <= bulletAmount; i++)
            {
                if(!OutOfBounds(i))
                    bulletCache[i].Draw();
            }
        }

        private bool OutOfBounds(int indexInCache)
        {

            if(bulletCache[indexInCache].Position.X < bulletCache[indexInCache].Texture.Width) return true;
            if (bulletCache[indexInCache].Position.X > viewportWidth) return true;
            if (bulletCache[indexInCache].Position.Y < -bulletCache[indexInCache].Texture.Height) return true;
            if (bulletCache[indexInCache].Position.Y > viewportHeight) return true;

            return false;
        }
    }
}
