﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GoGunRobo_Mono
{
   
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Player player;
        Enemy enemy1;
        Enemy enemy2;
        BulletManager bulletManager;

        public Game1()
        {
            // Awh yissssssss
            this.Window.Title = "Go!! GunRobo!";

            graphics = new GraphicsDeviceManager(this);

            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            // Change window to a 3:4 ratio get better vertical space
            graphics.PreferredBackBufferWidth = 240;
            graphics.PreferredBackBufferHeight = 320;
            graphics.ApplyChanges();

            // Place player in the center bottom of the screen
            player = new Player(new Vector2(GraphicsDevice.Viewport.Width / 2,
                GraphicsDevice.Viewport.Height - 50));
            // Set the bounds at which the player can move in
            player.AssignBoundary(GraphicsDevice.PresentationParameters.BackBufferWidth,
                GraphicsDevice.PresentationParameters.BackBufferHeight);

            // Place the prototype enemies in adjacent positions to the player, and just off screen
            enemy1 = new Enemy(new Vector2(GraphicsDevice.Viewport.Width * 0.25f, 0f));
            enemy2 = new Enemy(new Vector2(GraphicsDevice.Viewport.Width * 0.75f, 0f));
            bulletManager = new BulletManager();
            bulletManager.SetViewportBounds(GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height);
            player.BulletManagerReference = bulletManager;
            enemy1.BulletManagerRef = bulletManager;
            enemy2.BulletManagerRef = bulletManager;

            base.Initialize();
        }

        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // Send a reference of the SpriteBatch to THE parent class
            Entity.BatchReference = spriteBatch;

            // Protoplayer is used for all entities at the moment
            player.Texture = enemy1.Texture = enemy2.Texture = Content.Load<Texture2D>("Sprites/Protoplayer");
            bulletManager.LoadTextures(Content.Load<Texture2D>("Sprites/Protoplayer"));
        }

        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        protected override void Update(GameTime gameTime)
        {
            KeyboardState key = Keyboard.GetState();
            GamePadState pad = GamePad.GetState(PlayerIndex.One);

            // Handing off references to the Keyboard and Gamepad states
            player.KeyState = key;
            player.PadState = pad;

            // Game Specific Controls
            if (pad.Buttons.Back == ButtonState.Pressed || 
                key.IsKeyDown(Keys.Escape))
                Exit();

            enemy1.Update(gameTime);
            enemy2.Update(gameTime);
            bulletManager.UpdateAllBullets();
            player.Update();
            // DEBUG
            //if (player.DidCollide(enemy1)) System.Console.WriteLine("Hit enemy 1");
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Purple);

            spriteBatch.Begin();
            enemy1.Draw(Color.Red);
            enemy2.Draw(Color.Green);
            player.Draw(Color.DarkSlateBlue);
            // Bullets **need** to be seen at all times, so always make it the last thing drawn
            // as to be ontop of all other sprites
            bulletManager.DrawAllBullets();
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
